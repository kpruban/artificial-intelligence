import numpy as np
import random

# Parametry gry
grid_size = 5
n_actions = 4 # góra, dół, lewo, prawo
n_episodes = 100
epsilon = 0.1 # prawdopodobieństwo losowego ruchu
alpha = 0.1 # współczynnik uczenia się
gamma = 0.9 # współczynnik dyskontowania
# Inicjalizacja Q-table
q_table = np.zeros((grid_size, grid_size, n_actions))

# Definicja przeszkód
obstacles = np.array([
    [0, 0, 0, 1, 0], 
    [1, 1, 0, 1, 0], 
    [0, 0, 0, 0, 0], 
    [0, 1, 1, 1, 0], 
    [0, 0, 0, 0, 0]
])

# Funkcja sprawdzająca czy pozycja jest na planszy oraz czy to jest przeszkoda
def is_valid_position(x, y):
    if 0 <= x < grid_size and 0 <= y < grid_size and obstacles[x, y] == 0:
        return True
    return False

def get_next_position(state, action):
    x, y = state
    if action == 0: # góra
        x -= 1
    elif action == 1: # dół
        x += 1
    elif action == 2: # lewo
        y -= 1
    elif action == 3: # prawo
        y += 1

    if is_valid_position(x, y):
        return (x, y)
    return state

def choose_action(state):
    if random.uniform(0, 1) < epsilon:
        return random.randint(0, n_actions - 1) # losowy ruch
    return np.argmax(q_table[state[0], state[1]]) # najlepszy ruch

# Trening agenta
for episode in range(n_episodes):
    state = (0, 0) # początkowa pozycja
    while state != (grid_size - 1, grid_size - 1): # cel to dolny prawy róg
        action = choose_action(state)
        next_state = get_next_position(state, action)

        # Nagroda
        if next_state == (grid_size - 1, grid_size - 1):
            reward = 1
        elif next_state == state:
            reward = -0.2
        else:
            reward = -0.01

        # Aktualizacja Q-table
        best_next_action = np.argmax(q_table[next_state[0],next_state[1]])
        q_table[state[0], state[1], action] += alpha * (reward + gamma * q_table[next_state[0], next_state[1], best_next_action] - q_table[state[0], state[1], action])

        state = next_state

# Testowanie agenta
state = (0, 0)
path = [state]
while state != (grid_size - 1, grid_size - 1):
    action = np.argmax(q_table[state[0], state[1]])
    if action == 0: # góra
        direction = '# UP'
    elif action == 1: # dół
        direction = '# DOWN'
    elif action == 2: # lewo
        direction = '# LEFT'
    elif action == 3: # prawo
        direction = '# RIGHT'
    print(f"Agent ruch: {action}, pozycja: {state} {direction}")
    state = get_next_position(state, action)
    path.append(state)
    
print("Agent dotarł do celu!")

def print_path_on_grid(path, obstacles):
    grid = np.full((grid_size, grid_size), ' ')
    for i in range(grid_size):
        for j in range(grid_size):
            if obstacles[i, j] == 1:
                grid[i, j] = 'X'
    for x, y in path:
        grid[x, y] = '*'
    grid[0, 0] = 'S'
    x, y = path[-1]
    grid[x, y] = 'A'

    for row in grid:
        print(' '.join(row))


print("\nŚcieżka agenta:")
print_path_on_grid(path, obstacles)